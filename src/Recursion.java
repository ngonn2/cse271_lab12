/**
 * This class focuses on two recursion methods.
 * 
 * @author Nhu Ngo
 *
 */
public class Recursion {
    /*
     * Given base and n that are both 1 or more, compute recursively 
     *    (no loops) the value of base to the n power, so powerN(3, 2) 
     *     is 9 (3 squared).
     */
    public static int powerN(int base, int n) {
        if (n == 0) {
            return 1;
        }
        return base * powerN(base, n - 1);
    }

    /*
     * Given base and n that are both 1 or more, compute recursively 
     *     (no loops) the value of base to the n power, so powerN(3, 2) 
     *      is 9 (3 squared).
     */
    public static int triangle(int row) {
        if (row == 0) {
            return 0;
        }
        return triangle(row - 1) + row;
    }
}
